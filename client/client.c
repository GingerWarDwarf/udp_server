#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define SERV_PORT 9877
#define MAXLINE 4096


//
// Created by turbo_ginger on 6/13/18.
//
int main(int argc, char *argv[]) {
    struct sockaddr_in servaddr;
    memset (&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons (SERV_PORT);
    inet_pton (AF_INET, argv[1], &servaddr.sin_addr);
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);
    char sendline[MAXLINE];
    char recvline[MAXLINE];
    while(1) {
        memset(sendline,0,sizeof(sendline));
        memset(recvline,0,sizeof(recvline));
        fgets(sendline,MAXLINE,stdin);
        sendto(sockfd, sendline, strlen(sendline), 0, (struct sockaddr *) &servaddr, sizeof(servaddr));
        int num = recvfrom(sockfd, recvline, MAXLINE, 0, NULL, NULL);
        printf("Received:%s\n", recvline);
        fflush(stdout);
        //printf("answer:%s %d\n",recvline,num);
    }

}

